function decodeBlueprint(str) {
    //var binary = Base64.decode(str.substring(1))
    var encoded = atob(str.substring(1))
    //console.log("binary", encoded)
    var decoded = pako.inflate(encoded)
    //console.log("uzip", decoded)
    var string = new TextDecoder("utf-8").decode(decoded)	  
    //console.log("string", string)
    var jsonObject = JSON.parse(string)
    //console.log("jsonObj", jsonObject)
    var jsonString = JSON.stringify(jsonObject, null, 4)
    //console.log("jsonString", jsonString)
    return jsonObject
}